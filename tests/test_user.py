# test_user.py
import pytest
from user import User

def test_user_creation():
    user = User(1, "john_doe", "john@example.com")
    assert user.user_id == 1
    assert user.username == "john_doe"
    assert user.email == "john@example.com"

def test_user_to_dict():
    user = User(1, "john_doe", "john@example.com")
    user_dict = user.to_dict()
    assert isinstance(user_dict, dict)
    assert user_dict == {
        "user_id": 1,
        "username": "john_doe",
        "email": "john@example.com",
    }
