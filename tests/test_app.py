import pytest
from app import create_app

app = create_app()

def test_index_page():
    client = app.test_client()
    response = client.get('/')
    assert response.status_code == 200
    assert b"Welcome to My Flask App" in response.data

def test_user_controller():
    client = app.test_client()
    response = client.get('/user/john_doe')  # Ensure the URL path matches the route definition
    assert response.status_code == 200
    assert b"User Profile: john_doe" in response.data

def test_gitlab_api():
    client = app.test_client()
    response = client.get('/gitlab_data')  # Ensure the URL path matches the route definition
    assert response.status_code == 200
    assert b"GitLab Data" in response.data

@pytest.mark.parametrize("endpoint", ["projects", "users"])
def test_gitlab_api(endpoint):
    client = app.test_client()
    response = client.get(f'/gitlab_data?endpoint={endpoint}')
    assert response.status_code == 200
    assert b"GitLab Data" in response.data

# def test_invalid_gitlab_api():
#     client = app.test_client()
#     response = client.get('/gitlab_data?endpoint=invalid_endpoint')
#     assert response.status_code == 200  # You can modify this to handle errors
#     assert b
