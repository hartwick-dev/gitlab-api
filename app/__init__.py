from flask import Flask
from .config import EnvironmentConfig
from app.controllers.user_controller import bp as user_bp
from app.controllers.gitlab_controller import bp as gitlab_bp

def create_app():
    app = Flask(__name__)

    # Load environment variables from .env
    EnvironmentConfig()

    # Configure the app using your configuration class
    app.config.from_object('app.config.EnvironmentConfig')

    # Import and register routes
    from app import routes
    app.register_blueprint(routes.bp)
    app.register_blueprint(user_bp)
    app.register_blueprint(gitlab_bp)

    return app
