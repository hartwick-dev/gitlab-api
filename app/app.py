from flask import Flask
from .config import EnvironmentConfig

def create_app():
    app = Flask(__name__)

    # Load environment variables from .env
    EnvironmentConfig()

    # Configure the app using your configuration class
    app.config.from_object('app.config.EnvironmentConfig')

    # Import and register routes
    from app import routes
    app.register_blueprint(routes.bp)

    return app

if __name__ == "__main__":
    app = create_app()
    app.run()
