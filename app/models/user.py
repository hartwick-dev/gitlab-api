# user.py
class User:
    def __init__(self, user_id, username, email):
        """
        Initialize a User object.

        :param user_id: The ID of the user.
        :param username: The username of the user.
        :param email: The email address of the user.
        """
        self.user_id = user_id
        self.username = username
        self.email = email

    def to_dict(self):
        """
        Convert the User object to a dictionary.

        :return: A dictionary representation of the User.
        """
        return {
            "user_id": self.user_id,
            "username": self.username,
            "email": self.email,
        }
