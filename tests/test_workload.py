# test_workload.py
import pytest
from workload import Workload

@pytest.fixture
def workload_instance():
    # Create a Workload instance for testing
    user_id = 1  # Replace with an actual user ID
    gitlab_api = MockGitLabAPI()  # Replace with a mock or test-specific GitLab API
    return Workload(user_id, gitlab_api)

def test_get_merge_requests(workload_instance):
    # Test get_merge_requests method
    merge_requests = workload_instance.get_merge_requests()
    assert isinstance(merge_requests, list)

def test_get_active_chats(workload_instance):
    # Test get_active_chats method
    active_chats = workload_instance.get_active_chats()
    assert isinstance(active_chats, list)

def test_get_issues_and_labels(workload_instance):
    # Test get_issues_and_labels method
    issues, labels = workload_instance.get_issues_and_labels()
    assert isinstance(issues, list)
    assert isinstance(labels, list)

def test_calculate_workload(workload_instance):
    # Test calculate_workload method
    workload = workload_instance.calculate_workload()
    assert isinstance(workload, (int, float))

def test_get_workload_summary(workload_instance):
    # Test get_workload_summary method
    summary = workload_instance.get_workload_summary()
    assert isinstance(summary, dict)
    assert "merge_requests" in summary
    assert "active_chats" in summary
    assert "issues" in summary
    assert "weight" in summary
