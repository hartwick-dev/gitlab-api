import requests
import os

GITLAB_API_URL = "https://gitlab.com/api/v4/groups/75397075"  # Update this with your GitLab instance's URL

def get_gitlab_data(endpoint, params=None):
    api_token = os.getenv("GITLAB_API_TOKEN")
    headers = {
        "Private-Token": api_token,
    }

    response = requests.get(f"{GITLAB_API_URL}/{endpoint}", headers=headers, params=params)

    if response.status_code == 200:
        return response.json()
    else:
        return None  # You can handle errors as needed
