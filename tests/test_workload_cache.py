# test_workload_cache.py
import pytest
from workload import WorkloadCache

def test_workload_cache():
    # Create a WorkloadCache instance
    cache = WorkloadCache()

    # Test cache set and get
    user_id = 1
    data = {"workload": 42}
    cache.set_workload(user_id, data)
    cached_data = cache.get_workload(user_id)
    assert cached_data == data

    # Test cache miss
    non_existing_user_id = 99
    cached_data = cache.get_workload(non_existing_user_id)
    assert cached_data is None

    # Clean up
    cache.close()
