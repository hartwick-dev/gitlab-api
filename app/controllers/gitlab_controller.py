from flask import Blueprint, render_template
from app.gitlab_client import get_gitlab_data

bp = Blueprint("gitlab", __name__)

@bp.route('/gitlab_data')
def gitlab_data():
    endpoint = "projects"  # Specify the GitLab API endpoint you want to access
    data = get_gitlab_data(endpoint)

    if data is not None:
        # Process and display data as needed
        return render_template('gitlab_data.html', data=data)
    else:
        return "Error: Unable to fetch GitLab data"

# @bp.route('/gitlab_data')  # Make sure the route definition is accurate
# def gitlab_data():
#     # Controller logic to fetch GitLab data
#     return "GitLab Data"
