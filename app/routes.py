from flask import Blueprint

bp = Blueprint("app", __name__)

@bp.route('/')
def index():
    return "Welcome to My Flask App"

# Import and register other routes here
