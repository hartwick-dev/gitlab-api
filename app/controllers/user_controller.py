from flask import Blueprint

bp = Blueprint("user", __name__)

@bp.route("/user/<username>")
def user_profile(username):
    # Controller logic to retrieve user data
    return f"User Profile: {username}"
