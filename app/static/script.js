// Function to fetch data from the server and handle local storage caching
function fetchDataFromServer() {
    // Check if data is already cached in local storage
    const cachedData = localStorage.getItem('cachedData');

    if (cachedData) {
        // If data is found in local storage, use it
        displayData(JSON.parse(cachedData));
    } else {
        // If data is not cached, make an AJAX request to the server
        fetch('/api/data') // Replace with the appropriate endpoint
            .then(response => response.json())
            .then(data => {
                // Display the data on the web page
                displayData(data);

                // Cache the data in local storage for future use
                localStorage.setItem('cachedData', JSON.stringify(data));
            })
            .catch(error => {
                console.error('Error fetching data:', error);
            });
    }
}

// Function to display data on the web page
function displayData(data) {
    // Update the HTML elements with the data
    const outputElement = document.getElementById('data-output');
    outputElement.innerHTML = JSON.stringify(data, null, 2);
}

// Call the function to fetch and display data
fetchDataFromServer();
