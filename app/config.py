import os
from dotenv import load_dotenv

class EnvironmentConfig:
    def __init__(self):
        load_dotenv()

        self.SECRET_KEY = os.getenv("SECRET_KEY")
        self.DATABASE_URL = os.getenv("DATABASE_URL")
        self.GITLAB_API_TOKEN = os.getenv("GITLAB_API_TOKEN")
        # Add more configuration variables here

config = EnvironmentConfig()
