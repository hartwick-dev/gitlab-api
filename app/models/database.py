import sqlite3

class Database:
    def __init__(self, db_name):
        self.db_name = db_name

    def create_table(self):
        # Create a SQLite database and a table to store workload data
        conn = sqlite3.connect(self.db_name)
        cursor = conn.cursor()
        cursor.execute('''
            CREATE TABLE IF NOT EXISTS workload_data (
                user_id INTEGER PRIMARY KEY,
                workload REAL
            )
        ''')
        conn.commit()
        conn.close()

    def store_workload_data(self, user_id, workload):
        # Store workload data in the database
        conn = sqlite3.connect(self.db_name)
        cursor = conn.cursor()
        cursor.execute('INSERT OR REPLACE INTO workload_data (user_id, workload) VALUES (?, ?)', (user_id, workload))
        conn.commit()
        conn.close()

    def get_workload_data(self, user_id):
        # Retrieve workload data for a specific user
        conn = sqlite3.connect(self.db_name)
        cursor = conn.cursor()
        cursor.execute('SELECT workload FROM workload_data WHERE user_id = ?', (user_id,))
        result = cursor.fetchone()
        conn.close()
        return result[0] if result else None

    def get_all_workload_data(self):
        # Retrieve workload data for all users
        conn = sqlite3.connect(self.db_name)
        cursor = conn.cursor()
        cursor.execute('SELECT user_id, workload FROM workload_data')
        data = cursor.fetchall()
        conn.close()
        return {user_id: workload for user_id, workload in data}

    def drop_table(self):
        # Drop the workload data table
        conn = sqlite3.connect(self.db_name)
        cursor = conn.cursor()
        cursor.execute('DROP TABLE IF EXISTS workload_data')
        conn.commit()
        conn.close()

if __name__ == "__main__":
    # Example usage
    db = Database('workload.db')
    db.create_table()
    db.store_workload_data(1, 3.5)
    print(db.get_workload_data(1))  # Output: 3.5
