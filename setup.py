from setuptools import setup
from setuptools.command.install import install
import subprocess

class PostInstallCommand(install):
    def run(self):
        # Create a virtual environment
        subprocess.run(["python3", "-m", "venv", "venv"])
        
        # Activate the virtual environment and install dependencies
        subprocess.run(["venv/bin/pip", "install", "-r", "requirements.txt"])
        
        # Continue with the default install process
        install.run(self)

setup(
    name="your-flask-project",
    version="0.1",
    packages=["app"],
    install_requires=[],
    cmdclass={"install": PostInstallCommand},
)
